<!-- _coverpage.md -->

![logo](icon.svg)

# docsify <small>4.12.1</small>

> A magical documentation site generator modify by TCB 10.26 14:07.

- Simple and lightweight
- No statically built html files
- Multiple themes

[GitHub](https://github.com/docsifyjs/docsify/)
[Get Started](#headline)
